package com.myvouchee.sns.converter;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.myvouchee.sns.domain.Mail;

public class MailConverterTest {
	
	@Test
	public void Mail2JsonTests()
	{
		String origJson = "{\"from\":\"sender@gmail.com\",\"to\":\"reciever@gmail.com\",\"subject\":\"Test Subject\",\"msg\":\"Test Body\"}";
		try{
		Mail mail = new Mail("sender@gmail.com", "reciever@gmail.com", "Test Subject", "Test Body");
		String mailJson = MailConverter.Mail2Json(mail);
		System.out.println(mailJson);
		Assert.assertArrayEquals(origJson.getBytes(), mailJson.getBytes());
		}
		catch(Exception e)
		{
			Assert.fail(Arrays.deepToString(e.getStackTrace()));
		}
	}
}
