package com.myvouchee.sns.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.myvouchee.sns.domain.Mail;

@RunWith(SpringJUnit4ClassRunner.class)
//ApplicationContext will be loaded from "/applicationContext.xml" and "/applicationContext-test.xml"
//in the root of the classpath
@ContextConfiguration({"classpath:applicationContext.xml"})
public class MailServiceImplTest {

	@Autowired
	MailService mailService;
	
	@Test
	public void deliverMailTests()
	{
		mailService.sendMail(new Mail("jitendra.chauhan@gmail.com", "jitendra.chauhan@gmail.com", 
				"Test Subject", "Test Msg"));
	}
}
