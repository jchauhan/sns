package com.myvouchee.vcs.domain;

import java.util.UUID;

import junit.framework.Assert;

import org.junit.Test;

import com.myvouchee.commons.exception.JmsMessageGenerationException;
import com.myvouchee.commons.exception.JmsMessageParsingException;
import com.myvouchee.vcs.converter.PayloadToJobDetails;
import com.myvouchee.vcs.domain.job.Job;
import com.myvouchee.vcs.domain.job.JobType;
import com.myvouchee.vcs.domain.job.PayloadType;
import com.myvouchee.vcs.domain.job.fb.FetchFbFriendListJobDetail;

public class JobTest {

	@Test
	public void deliverMailTests()
	{
		FetchFbFriendListJobDetail detail = new FetchFbFriendListJobDetail();
		detail.setAccessToken("AAAAAAITEghMBAAjwsIcx5JZCVMcJVNvHhmU01Yc5LmIaPuhQuMKaQSZAAhkaufzCZB4QnWbnarVLUukqJk8nmRuqhd0S8cPL3F7QOfAzgZDZD");
		try {
			/*
			 * Conversion to Json
			 */
			String detailJson = PayloadToJobDetails.toJson(detail);
			System.out.println(detailJson);
			Job job = new Job();
			job.setId(UUID.randomUUID().toString());
			job.setPayloadType(PayloadType.Content);
			job.setType(JobType.FetchFbFriendsList);
			job.setPayload(detailJson);
			String finalJson = 	 PayloadToJobDetails.toJson(job);
			System.out.println(finalJson);


			/*
			 * Converting back from json
			 */
			Job job2 = (Job)PayloadToJobDetails.fromJson(finalJson, Job.class);
			FetchFbFriendListJobDetail detail2 = (FetchFbFriendListJobDetail)PayloadToJobDetails.fromJson(job2.getPayload(), FetchFbFriendListJobDetail.class);
			Assert.assertEquals(detail.getAccessToken(), detail2.getAccessToken());

		} catch (JmsMessageGenerationException e) {
			Assert.fail("Failed to convert to Json");
		}
		catch (JmsMessageParsingException e) {
			Assert.fail("Failed to parsing from Json");
		}
	}
}
