package com.myvouchee.commons.jms;

import javax.jms.JMSException;  
import javax.jms.Queue;  
import org.apache.log4j.Logger;  
import org.springframework.jms.core.JmsTemplate;  

/**  
 * The MailResponseMessageSender class uses the injected JMSTemplate to send a message  
 * to a specified Queue. In our case we're sending messages to 'TestQueueTwo'  
 */    
public class AbstractQueueMessageSender  
{  
	private JmsTemplate jmsTemplate;  
	private Queue queue;  
	private static final Logger logger_c = Logger .getLogger(AbstractQueueMessageSender.class);  

	/**  
	 * Sets the jms template.  
	 *  
	 * @param template the jms template  
	 */  
	public void setJmsTemplate(JmsTemplate tmpl)  
	{  
		this.jmsTemplate = tmpl;  
	}  

	/**  
	 * Sets the test queue.  
	 *  
	 * @param queue the new test queue  
	 */  
	public void setQueue(Queue queue)  
	{  
		this.queue = queue;  
	} 
	
	/**  
	 * Sends message using JMS Template.  
	 *  
	 * @param message_p the message_p  
	 * @throws JMSException the jMS exception  
	 */  
	public void sendMessage(String message_p) throws JMSException  
	{  
		logger_c.debug("About to put message on queue. Queue[" + queue.toString() + "] Message[" + message_p + "]");  
		jmsTemplate.convertAndSend(queue, message_p);  
	}   
}  
