package com.myvouchee.commons.exception;


public class JmsMessageGenerationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7180811973424659490L;

	public JmsMessageGenerationException() {
		super();
	}

	public JmsMessageGenerationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public JmsMessageGenerationException(String arg0) {
		super(arg0);
	}

	public JmsMessageGenerationException(Throwable arg0) {
		super(arg0);
	}

}
