package com.myvouchee.commons.exception;


public class JmsMessageParsingException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7180811973424659490L;

	public JmsMessageParsingException() {
		super();
	}

	public JmsMessageParsingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public JmsMessageParsingException(String arg0) {
		super(arg0);
	}

	public JmsMessageParsingException(Throwable arg0) {
		super(arg0);
	}

}
