package com.myvouchee.sns.jms;


import javax.jms.JMSException;  
import javax.jms.Message;  
import javax.jms.MessageListener;  
import javax.jms.TextMessage;  
import org.apache.log4j.Logger;  
import org.springframework.beans.factory.annotation.Autowired;

import com.myvouchee.commons.exception.JmsMessageGenerationException;
import com.myvouchee.commons.exception.JmsMessageParsingException;
import com.myvouchee.sns.converter.JmsMailRequestConverter;
import com.myvouchee.sns.converter.JmsRequestResponseConverter;
import com.myvouchee.sns.domain.JmsMailRequest;
import com.myvouchee.sns.domain.JmsRequestResponse;
import com.myvouchee.sns.domain.JmsResponseStatus;
import com.myvouchee.sns.service.MailService;

/**  
 * Class handles incoming messages  
 *  
 * @see PointOfIssueMessageEvent  
 */  
public class MailMessageListener implements MessageListener  
{  
	
	@Autowired
	protected MailService mailService;
	
	private MailResponseMessageSender mailResponseMessageSender;  
	private static final Logger logger_c = Logger.getLogger(MailMessageListener.class);  

	/**  
	 * Sets the message sender.  
	 *  
	 * @param messageSender_p the new message sender  
	 */  
	public void setMailResponseMessageSender(MailResponseMessageSender messageSender_p)  
	{  
		this.mailResponseMessageSender = messageSender_p;  
	}  
	
	/**  
	 * Method implements JMS onMessage and acts as the entry  
	 * point for messages consumed by Springs DefaultMessageListenerContainer.  
	 * When DefaultMessageListenerContainer picks a message from the queue it  
	 * invokes this method with the message payload.  
	 */  
	public void onMessage(Message message)  
	{  
		logger_c.debug("Received message from queue [" + message +"]");  
		JmsRequestResponse response = new JmsRequestResponse();
		String errMsg = "";
		String code = "";
		/* The message must be of type TextMessage */  
		if (message instanceof TextMessage)  
		{  
			try  
			{  
				String msgText = ((TextMessage) message).getText();  
				logger_c.debug("About to process message: " + msgText);  
				JmsMailRequest request = JmsMailRequestConverter.Json2JmsMailRequest(msgText);
				
				response.setId(request.getId());
				mailService.sendMail(request.getData());
				/* call message sender to put message onto second queue */  
				code = JmsResponseStatus.SUCCESS.toString();
			}  
			catch (JMSException jmsEx_p)  
			{  
				errMsg = "An error occurred extracting message";  
				logger_c.error(errMsg, jmsEx_p);  
				code = JmsResponseStatus.FAILED.toString();
				
			} catch (JmsMessageParsingException e) {
				errMsg = "An error occurred extracting message";  
				logger_c.error(errMsg, e);  
				code = JmsResponseStatus.MSG_PARSING_ERROR.toString();
			} 
		}  
		else  
		{  
			errMsg = "Message is not of expected type TextMessage"; 
			code = JmsResponseStatus.MSG_PARSING_ERROR.toString();
			logger_c.error(errMsg);  
			throw new RuntimeException(errMsg);  
		}  
		response.setMsg(errMsg);
		response.setCode(code);
		
		try {
			mailResponseMessageSender.sendMessage(JmsRequestResponseConverter.JmsRequestResponse2Json(response));
		} catch (JMSException e) {
			logger_c.error(e);  
		} catch (JmsMessageGenerationException e) {
			logger_c.error(e);  			
		}  
	}  
}  
