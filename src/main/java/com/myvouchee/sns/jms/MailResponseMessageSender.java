package com.myvouchee.sns.jms;

import com.myvouchee.commons.jms.AbstractQueueMessageSender;
 

/**  
 * The MailResponseMessageSender class uses the injected JMSTemplate to send a message  
 * to a specified Queue. In our case we're sending messages to 'TestQueueTwo'  
 */    
public class MailResponseMessageSender  extends AbstractQueueMessageSender
{
	public MailResponseMessageSender() {
		super();
	}  
}  
