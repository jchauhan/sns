package com.myvouchee.sns.converter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.myvouchee.sns.domain.Mail;

public class MailConverter {
	public static String Mail2Json(Mail mail) throws JsonGenerationException, JsonMappingException, IOException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(baos, mail);
		return new String(baos.toByteArray());
	}
	
	public static Mail Json2Mail(String json) throws JsonParseException, JsonMappingException, IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json.getBytes(), Mail.class);
	}
}
