package com.myvouchee.sns.converter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.myvouchee.commons.exception.JmsMessageGenerationException;
import com.myvouchee.commons.exception.JmsMessageParsingException;
import com.myvouchee.sns.domain.JmsMailRequest;

public class JmsMailRequestConverter {
	public static String JmsMailRequest2Json(JmsMailRequest mail) throws JmsMessageGenerationException 
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(baos, mail);
		} catch (JsonGenerationException e) {
			throw new JmsMessageGenerationException(e);
		} catch (JsonMappingException e) {
			throw new JmsMessageGenerationException(e);
		} catch (IOException e) {
			throw new JmsMessageGenerationException(e);
		}
		return new String(baos.toByteArray());
	}
	
	public static JmsMailRequest Json2JmsMailRequest(String json) throws JmsMessageParsingException
	{
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json.getBytes(), JmsMailRequest.class);
		} catch (JsonParseException e) {
			throw new JmsMessageParsingException(e);
		} catch (JsonMappingException e) {
			throw new JmsMessageParsingException(e);
		} catch (IOException e) {
			throw new JmsMessageParsingException(e);
		}
	}
}
