package com.myvouchee.sns.domain;

public class JmsMailRequest {

	String id;
	Mail data;
	
	public JmsMailRequest() {
		super();
	}
	
	public JmsMailRequest(String id, Mail mail) {
		super();
		this.id = id;
		this.data = mail;
	}

	public Mail getData() {
		return data;
	}

	public void setData(Mail data) {
		this.data = data;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
