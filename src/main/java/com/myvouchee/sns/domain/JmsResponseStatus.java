package com.myvouchee.sns.domain;

public enum JmsResponseStatus {
	SUCCESS, FAILED, UNKNOWN, MSG_PARSING_ERROR
}
