package com.myvouchee.sns.domain;

public class JmsRequestResponse {
	String id;
	String code;
	String msg;
	
	public JmsRequestResponse() {
		super();
	}
	
	public JmsRequestResponse(String id, String code, String msg) {
		super();
		this.id = id;
		this.code = code;
		this.msg = msg;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
}
