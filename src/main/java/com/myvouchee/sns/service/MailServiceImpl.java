package com.myvouchee.sns.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import com.myvouchee.sns.domain.Mail;

/**
 * @author Jitendra Chauhan
 * 
 */
@Service
public class MailServiceImpl implements MailService
{
	private MailSender mailSender;

	@Autowired
	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void sendMail(Mail mail) 
	{
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(mail.getFrom());
		message.setTo(mail.getTo());
		message.setSubject(mail.getSubject());
		message.setText(mail.getMsg());
		mailSender.send(message);	
	}
}
