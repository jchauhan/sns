package com.myvouchee.sns.service;

import com.myvouchee.sns.domain.Mail;

/**
 * @author Jitendra Chauhan
 * 
 */
public interface MailService {
	/*
	 * Deliver the mail to mail server configured
	 * @param
	 */
	public void sendMail(Mail mail);
}
