package com.myvouchee.vcs.jms;

import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;

import com.myvouchee.commons.jms.AbstractMessageListener;

public abstract class AbstractJobQMessageListener extends AbstractMessageListener
{
	@Autowired
	Properties errorProperties;
	
	
	protected String getPropertyValue(String key)
	{
		return errorProperties.getProperty(key);
	}
	
	
	
}
