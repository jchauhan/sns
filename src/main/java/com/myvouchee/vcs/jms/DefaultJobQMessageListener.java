package com.myvouchee.vcs.jms;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.springframework.social.facebook.api.FriendOperations;
import org.springframework.social.facebook.api.Reference;
import org.springframework.social.facebook.api.impl.FacebookTemplate;

import com.myvouchee.commons.exception.JmsMessageGenerationException;
import com.myvouchee.commons.exception.JmsMessageParsingException;
import com.myvouchee.commons.exception.UnhandledJobTypeException;
import com.myvouchee.vcs.converter.PayloadToJobDetails;
import com.myvouchee.vcs.domain.job.Job;
import com.myvouchee.vcs.domain.job.JobResult;
import com.myvouchee.vcs.domain.job.JobType;
import com.myvouchee.vcs.domain.job.PayloadType;
import com.myvouchee.vcs.domain.job.fb.FetchFbFriendListJobDetail;
import com.myvouchee.vcs.domain.job.fb.FetchFbUserProfileImageJobDetail;
import com.myvouchee.vcs.domain.job.fb.FetchFbUserProfileJobDetail;

/*
 * @author Jitendra Chauhan
 */
public class DefaultJobQMessageListener implements MessageListener  {
	private static final Logger logger_c = Logger.getLogger(DefaultJobQMessageListener.class);  
	
	DefaultJobQResponseSender defaultJobQResponseSender;
	
	
	public DefaultJobQResponseSender getDefaultJobQResponseSender() {
		return defaultJobQResponseSender;
	}

	public void setDefaultJobQResponseSender(
			DefaultJobQResponseSender defaultJobQResponseSender) {
		this.defaultJobQResponseSender = defaultJobQResponseSender;
	}

	@Override
	public void onMessage(Message message) {
		JobResult response = new JobResult();
		Job job2 = null;
		if (message instanceof TextMessage)  
		{  
			try  
			{  
				String msgText = ((TextMessage) message).getText();  
				logger_c.debug("About to process message: " + msgText);  
				job2 = (Job)PayloadToJobDetails.fromJson(msgText, Job.class);
				String payload = handleJob(job2);
				response = createJobResult(job2.getId(), job2.getType(), PayloadType.Content, payload);

			}  
			catch (JMSException e)  
			{  
				response = createJobResult(extractJobId(job2), extractJobType(job2), PayloadType.Content, e.getMessage());
			} catch (JmsMessageParsingException e) 
			{
				response = createJobResult(extractJobId(job2), extractJobType(job2), PayloadType.Content, e.getMessage());
			} catch (JmsMessageGenerationException e) {
				response = createJobResult(extractJobId(job2), extractJobType(job2), PayloadType.Content, e.getMessage());
			} catch(Exception e)
			{
				response = createJobResult(extractJobId(job2), extractJobType(job2), PayloadType.Content, e.getMessage());	
			}
			
		}  
		else  
		{  
			response = createJobResult(extractJobId(job2), extractJobType(job2), PayloadType.Content, "Msg is unknown format");

			//need to handle
		}  
		try {
			defaultJobQResponseSender.sendMessage(PayloadToJobDetails.toJson(response));
		} catch (JMSException e) {
			//TODO may be need to push to error Q
			logger_c.error("Error in sending response to  response Q" + e.getMessage(), e);  
		} catch (JmsMessageGenerationException e) {
			//TODO may be need to push to error Q
			logger_c.error("Error in generating Jms msg" + e.getMessage(), e);  
		}
		catch(Exception e)
		{
			logger_c.error("Internal Error " + e.getMessage(), e);  
		}
	}
	
	protected String extractJobId(Job job)
	{
		return job == null? "" : job.getId();
	}
	
	protected JobType extractJobType(Job job)
	{
		return job == null? JobType.Unknown : job.getType();
	}
	
	protected JobResult createJobResult(String id, JobType type, PayloadType payloadType, String payload)
	{
		JobResult response = new JobResult();
		response.setId(id);
		response.setType(type);
		response.setPayloadType(payloadType);
		response.setPayload(payload);
		return response;
	}
	
	protected String handleJob(Job job) throws JmsMessageParsingException, JmsMessageGenerationException, UnhandledJobTypeException
	{
		switch(job.getType())
		{
			case FetchFbFriendsList:
				return handleFetchFbFriendsListJob(job);
			case FetchUserProfile:
				return handleFetchUserProfile(job);
			case FetchUserProfileImage:
				return handleFetchUserProfileImage(job);
			default:
				throw new UnhandledJobTypeException();
		}
	}
	
	
	protected String handleFetchFbFriendsListJob(Job job) throws JmsMessageParsingException, JmsMessageGenerationException
	{
		FetchFbFriendListJobDetail detial = (FetchFbFriendListJobDetail)job.createParsedPayload();
		FacebookTemplate facebook = new FacebookTemplate(detial.getAccessToken());
		FriendOperations friendOperations = facebook.friendOperations();
		List<Reference> references = null;
		if(detial.getUserId().trim().isEmpty())
			references = friendOperations.getFriends();
		else
			references = friendOperations.getFriends(detial.getUserId());
		
		return PayloadToJobDetails.toJson(references);
	}
	
	protected String handleFetchUserProfile(Job job) throws JmsMessageGenerationException, JmsMessageParsingException
	{
		FetchFbUserProfileJobDetail detial = (FetchFbUserProfileJobDetail)job.createParsedPayload();
		FacebookTemplate facebook = new FacebookTemplate(detial.getAccessToken());
		Object profile = null;
		if(isNullOrEmpty(detial.getUserId()))
			profile = facebook.userOperations().getUserProfile();
		else
			profile = facebook.userOperations().getUserProfile(detial.getUserId());

		return PayloadToJobDetails.toJson(profile);
	}
	
	protected String handleFetchUserProfileImage(Job job) throws JmsMessageParsingException, JmsMessageGenerationException
	{
		FetchFbUserProfileImageJobDetail detial = (FetchFbUserProfileImageJobDetail)job.createParsedPayload();
		FacebookTemplate facebook = new FacebookTemplate(detial.getAccessToken());
		Object profile = null;
		if(isNullOrEmpty(detial.getUserId()))
			profile = facebook.userOperations().getUserProfileImage(detial.getImageType());
		else
			profile = facebook.userOperations().getUserProfileImage(detial.getUserId(), detial.getImageType());

		return PayloadToJobDetails.toJson(profile);
	}
	
	
	protected boolean isNullOrEmpty(String str)
	{
		return str == null || (str.trim().isEmpty());
	}

}
