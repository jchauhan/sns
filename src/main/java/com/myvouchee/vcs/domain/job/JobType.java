package com.myvouchee.vcs.domain.job;

/*
 * @author Jitendra Chauhan
 */
public enum JobType {
	FetchFbFriendsList,
	FetchUserProfile,
	FetchUserProfileImage,
	Unknown
}
