package com.myvouchee.vcs.domain.job.fb;

public class BaseFbJobDetail {
	String accessToken;
	
	public BaseFbJobDetail() {
		super();
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
}
