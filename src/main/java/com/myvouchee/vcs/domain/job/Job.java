package com.myvouchee.vcs.domain.job;

import org.apache.commons.lang.NotImplementedException;

import com.myvouchee.commons.exception.JmsMessageParsingException;
import com.myvouchee.vcs.converter.PayloadToJobDetails;
import com.myvouchee.vcs.domain.job.fb.FetchFbFriendListJobDetail;
import com.myvouchee.vcs.domain.job.fb.FetchFbUserProfileImageJobDetail;
import com.myvouchee.vcs.domain.job.fb.FetchFbUserProfileJobDetail;

/*
 * @author Jitendra Chauhan
 */
public class Job{
	JobType type;
	String id;
	PayloadType payloadType;
	protected String payload;
	
	public Job() {
		super();
	}
	
	public JobType getType() {
		return type;
	}
	public void setType(JobType type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	public PayloadType getPayloadType() {
		return payloadType;
	}
	
	public void setPayloadType(PayloadType payloadType) {
		this.payloadType = payloadType;
	}
	
	public Object createParsedPayload() throws JmsMessageParsingException
	{
		String content = "";
		switch(payloadType)
		{
		case Content:
			content = payload;
			break;
		case URI:
			throw new NotImplementedException("Need to implement");
		default:
			throw new NotImplementedException("Need to implement");
		}
		
		switch(type)
		{
			case FetchFbFriendsList:
				return (FetchFbFriendListJobDetail)PayloadToJobDetails.fromJson(content, FetchFbFriendListJobDetail.class);
			case FetchUserProfile:
				return (FetchFbUserProfileJobDetail)PayloadToJobDetails.fromJson(content, FetchFbUserProfileJobDetail.class);
			case FetchUserProfileImage:
				return (FetchFbUserProfileImageJobDetail)PayloadToJobDetails.fromJson(content, FetchFbUserProfileImageJobDetail.class);
			default:
				throw new NotImplementedException("Need to implement");
		}
	}
	
}
