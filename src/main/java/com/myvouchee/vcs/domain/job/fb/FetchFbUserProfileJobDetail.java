package com.myvouchee.vcs.domain.job.fb;

public class FetchFbUserProfileJobDetail extends BaseFbJobDetail
{
	/*
	 * User Id for whose friends list id requested.
	 */
	String userId = "";
	
//	/*
//	 * offset - the offset into the friends list to start retrieving profiles.
//	 */
//	int offset = 0;
//
//	/*
//	 * limit - the maximum number of profiles to return.
//	 */
//	int limit = 100;

	public FetchFbUserProfileJobDetail() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId.trim();
	}
}
