package com.myvouchee.vcs.domain.job;

public enum PayloadType {
	Content, URI
}
