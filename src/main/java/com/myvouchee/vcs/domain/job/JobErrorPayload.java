package com.myvouchee.vcs.domain.job;

public class JobErrorPayload 
{
	//Error code describing type of error
	int code;
	//Short Error Msg
	String msg;
	//Error Details. Can be used to pass on exceptions
	String details;
	
	public JobErrorPayload() {
		super();
	}
	
	public JobErrorPayload(int code, String msg, String details) {
		super();
		this.code = code;
		this.msg = msg;
		this.details = details;
	}

	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
}
