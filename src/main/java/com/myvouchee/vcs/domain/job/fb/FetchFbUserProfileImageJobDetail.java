package com.myvouchee.vcs.domain.job.fb;

import org.springframework.social.facebook.api.ImageType;

public class FetchFbUserProfileImageJobDetail extends BaseFbJobDetail
{
	/*
	 * User Id for whose friends list id requested.
	 */
	String userId = "";
	ImageType imageType = ImageType.NORMAL;
	
//	/*
//	 * offset - the offset into the friends list to start retrieving profiles.
//	 */
//	int offset = 0;
//
//	/*
//	 * limit - the maximum number of profiles to return.
//	 */
//	int limit = 100;

	public FetchFbUserProfileImageJobDetail() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId.trim();
	}

	public ImageType getImageType() {
		return imageType;
	}

	public void setImageType(ImageType imageType) {
		this.imageType = imageType;
	}
	
}
