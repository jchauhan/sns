package com.myvouchee.vcs.converter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.myvouchee.commons.exception.JmsMessageGenerationException;
import com.myvouchee.commons.exception.JmsMessageParsingException;

public class PayloadToJobDetails {
	public static Object fromJson(String payload, Class<?> _class) throws JmsMessageParsingException
	{
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(payload.getBytes(), _class);
		} catch (JsonParseException e) {
			throw new JmsMessageParsingException(e);
		} catch (JsonMappingException e) {
			throw new JmsMessageParsingException(e);
		} catch (IOException e) {
			throw new JmsMessageParsingException(e);
		}
	}
	
	public static  String toJson(Object object) throws JmsMessageGenerationException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(baos, object);
		} catch (JsonGenerationException e) {
			throw new JmsMessageGenerationException(e);
		} catch (JsonMappingException e) {
			throw new JmsMessageGenerationException(e);
		} catch (IOException e) {
			throw new JmsMessageGenerationException(e);
		}
		return new String(baos.toByteArray());
	}
	
}
