It is a notification platform to support following 
1. Send emails to destination
2. Send SMS to destination mobile Number
Currently it listens to an ActiveMQ queues

Integration with ActiveMQ is inspired with http://briansjavablog.blogspot.in/2012/09/spring-jms-tutorial-with-activemq.html

How to Run:
============

= Setup ActiveMQ (If not already there) 
== Getting Started 
=== Installation 
 Please go through active getting started documentation {{ Getting Started | http://activemq.apache.org/getting-started.html}}

== Conceptual 
 Please read through {{Concepts | http://fusesource.com/docs/broker/5.3/getting_started/FuseMBStartedKeyJMS.html}}


==Create following queues with using URL {{ActiveMQ Admin | http://localhost:8161/admin/queues.jsp}} 
* emailDeliveryRequestQ
* emailDeliveryResponseQ
*portalVcsJobRequestQ
*portalVcsJobResponseQ

= Package as war

run: 'mvn clean package' 
and deploy target/sns.war to tomcat webapp directory

= How to test
You should see consumers and should be able to send message using url {{ActiveMQ Admin | http://localhost:8161/admin/queues.jsp}}

The message format should be json. Please do not change sender and MUST change reciepent 

{"id":"someid",
"data":{"from":"jitsincha@gmail.com","to":"reciever@gmail.com","subject":"Test Subject","msg":"Test Body"}
}

